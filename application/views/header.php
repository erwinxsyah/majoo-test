
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <title>Majoo Technology</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">


    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="<?php echo base_url('asset/assets/css/fontawesome.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('asset/assets/css/templatemo-onix-digital.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('asset/assets/css/animated.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/owl.css');?>">
	
	
<!--Less styles -->
   <!-- Other Less css file //different less files has different color scheam
	<link rel="stylesheet/less" type="text/css" href="themes/less/simplex.less">
	<link rel="stylesheet/less" type="text/css" href="themes/less/classified.less">
	<link rel="stylesheet/less" type="text/css" href="themes/less/amelia.less">  MOVE DOWN TO activate
	-->
	<!--<link rel="stylesheet/less" type="text/css" href="themes/less/bootshop.less">
	<script src="themes/js/less.js" type="text/javascript"></script> -->
	
<!-- Bootstrap style --> 
    <link id="callCss" rel="stylesheet" href="<?php echo base_url('assets/bootshop/themes/bootshop/bootstrap.min.css');?>" media="screen"/>
    <link href="<?php echo base_url('assets/bootshop/themes/css/base.css');?>" rel="stylesheet" media="screen"/>
<!-- Bootstrap style responsive -->	
	<link href="<?php echo base_url('assets/bootshop/themes/css/bootstrap-responsive.min.css');?>" rel="stylesheet"/>
	<link href="<?php echo base_url('assets/bootshop/themes/css/font-awesome.css');?>" rel="stylesheet" type="text/css">
	
	 <!-- Scripts -->
  <script src="<?php echo base_url('asset/vendor/jquery/jquery.min.js');?>"></script>
  <script src="<?php echo base_url('asset/vendor/bootstrap/js/bootstrap.bundle.min.js');?>"></script>
  <script src="<?php echo base_url('asset/assets/js/owl-carousel.js');?>"></script>
  <script src="<?php echo base_url('asset/assets/js/animation.js');?>"></script>
  <script src="<?php echo base_url('asset/assets/js/imagesloaded.js');?>"></script>
  <script src="<?php echo base_url('asset/assets/js/custom.js');?>"></script>

  <script>
  // Acc
    $(document).on("click", ".naccs .menu div", function() {
      var numberIndex = $(this).index();

      if (!$(this).is("active")) {
          $(".naccs .menu div").removeClass("active");
          $(".naccs ul li").removeClass("active");

          $(this).addClass("active");
          $(".naccs ul").find("li:eq(" + numberIndex + ")").addClass("active");

          var listItemHeight = $(".naccs ul")
            .find("li:eq(" + numberIndex + ")")
            .innerHeight();
          $(".naccs ul").height(listItemHeight + "px");
        }
    });
  </script>
