
  <!-- ***** Header Area Start ***** -->
  <header class="header-area header-sticky wow slideInDown" data-wow-duration="0.75s" data-wow-delay="0s">

    <div class="container">
      <div class="row">
        <div class="col-12">
          <nav class="main-nav">
            <!-- ***** Logo Start ***** -->
			
            <a href="<?php echo site_url('welcome');?>" class="logo">
              <img src="<?php echo base_url('asset/assets/images/logo.png')?>">
            </a>
            <!-- ***** Logo End ***** -->
            <!-- ***** Menu Start ***** -->
		
            <ul class="nav">
			
              <li class="scroll-to-section"><a href="#top" class="active">Home</a></li>
              <li class="scroll-to-section"><a href="#about">Produk</a></li>
              <li class="scroll-to-section"><a href="#contact">Contact Us</a></li> 
			   <li class="scroll-to-section"><a href="<?php echo site_url('welcome/konfirmasi');?>">Konfirmasi Pembayaran</a></li> 
              <li class="scroll-to-section"><div class="main-red-button-hover"><a href="<?php echo site_url('login');?>">Login</a></div></li> 
			  
            </ul>        
            <a class='menu-trigger'>
                <span>Menu</span>
            </a>
            <!-- ***** Menu End ***** -->
          </nav>
        </div>
      </div>
    </div>
	<div class="container">
		<form class="form-inline navbar-search" method="post" action="<?php echo site_url('welcome/cariproduk');?>" >
		<span>Pencarian </span><input class="form-control" name="str" type="text">
		  <select class="form-control" name="kategori">
			<option>Standar</option>
			<option>Paket</option>
		</select> 
		  <button type="submit" id="submitButton" class="btn btn-success">Go</button>
    </form>
	<div id="welcomeLine" class="row">
	<div class="span6">
	<div class="pull-right">
		
		<a href="<?php echo site_url('welcome/cart');?>"><span class="btn btn-warning"><i class="icon-shopping-cart icon-white"></i> [ <?php echo $this->cart->total_items();?> ] Keranjang Belanja </span> </a> 
	</div>
	</div>
</div>
  </header>
  <!-- ***** Header Area End ***** -->
<div id="login" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="false" >
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3>Login Block</h3>
		  </div>
		  <div class="modal-body">
			<form class="form-horizontal loginFrm">
			  <div class="control-group">								
				<input type="text" id="inputEmail" placeholder="Email">
			  </div>
			  <div class="control-group">
				<input type="password" id="inputPassword" placeholder="Password">
			  </div>
			  <div class="control-group">
				<label class="checkbox">
				<input type="checkbox"> Remember me
				</label>
			  </div>
			</form>		
			<button type="submit" class="btn btn-success">Sign in</button>
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		  </div>
	</div>