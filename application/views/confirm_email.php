<!DOCTYPE html>
<html lang="en">
  <head>
    <?php $this->load->view('header');?>
  </head>

<!-- Navbar ================================================== -->
<?php $this->load->view('navbar');?>
</div>
</div>
<br>
<!-- Header End====================================================================== -->

<div id="mainBody">
	<div class="container">
	<div class="row">
<!-- Sidebar ================================================== -->
<?php $this->load->view('sidebar');?>
<!-- Sidebar end=============================================== -->
<div id="about" class="our-services section">

    <div class="services-right-dec">
      <img src="<?php echo base_url('asset/assets/images/services-right-dec.png');?>" alt="">
    </div>
    <div class="container">
      <div class="services-left-dec">
        <img src="<?php echo base_url('asset/assets/images/services-left-dec.png');?>" alt="">
      </div>

	<div class="row">
		<div class="span4 offset2">
		
			<form id="contact" action="<?php echo site_url('welcome/prosestransaksi');?>" method="post">
			
			  <div class="control-group">
				<label class="control-label" for="inputEmail0">Nama</label>
				<div class="controls">
				  <input name="nama" class="span3"  type="name" id="inputEmail0" placeholder="Nama" required>
				</div>
			  </div>
			  <div class="control-group">
				<label class="control-label" for="inputEmail0">No. Handphone</label>
				<div class="controls">
				  <input name="nope" class="span3"  type="number" id="inputEmail0" placeholder="No HP" required>
				</div>
			  </div>
			  <div class="control-group">
				<label class="control-label" for="inputEmail0">Alamat Lengkap</label>
				<div class="controls">
				  <textarea name="alamat" class="span3" required></textarea>
				</div>
			  </div>
			  <div class="controls">
			   <button type="submit" id="form-submit" class="main-button">Submit Request</button>
			  </div>
			  
			</form>
			
		</div>
		<div class="owl-carousel owl-banner">
		 <div class="item header-text">
                 	<h3>HARAP ISI BIODATA</h3>
			<p>Gunakan <em>No Handphone</em> yang valid untuk <span>pemberitahuan</span></p>
                  <p>Get your best website.</p>
				  </div>
				  </div>
		</div>
		
		<div class="span1"> &nbsp;</div>
	</div>	
	
</div>
</div></div>
</div>

<!-- Footer ================================================================== -->
	<div  id="footerSection">
	<div class="container">
		<div class="row">
			<div class="span3">
				<h5>ACCOUNT</h5>
				<a href="login.html">YOUR ACCOUNT</a>
				<a href="login.html">PERSONAL INFORMATION</a> 
				<a href="login.html">ADDRESSES</a> 
				<a href="login.html">DISCOUNT</a>  
				<a href="login.html">ORDER HISTORY</a>
			 </div>
			<div class="span3">
				<h5>INFORMATION</h5>
				<a href="contact.html">CONTACT</a>  
				<a href="register.html">REGISTRATION</a>  
				<a href="legal_notice.html">LEGAL NOTICE</a>  
				<a href="tac.html">TERMS AND CONDITIONS</a> 
				<a href="faq.html">FAQ</a>
			 </div>
			<div class="span3">
				<h5>OUR OFFERS</h5>
				<a href="#">NEW PRODUCTS</a> 
				<a href="#">TOP SELLERS</a>  
				<a href="special_offer.html">SPECIAL OFFERS</a>  
				<a href="#">MANUFACTURERS</a> 
				<a href="#">SUPPLIERS</a> 
			 </div>
			<div id="socialMedia" class="span3 pull-right">
				<h5>SOCIAL MEDIA </h5>
				<a href="#"><img width="60" height="60" src="<?php echo base_url('assets/bootshop/themes/images/facebook.png');?>" title="facebook" alt="facebook"/></a>
				<a href="#"><img width="60" height="60" src="<?php echo base_url('assets/bootshop/themes/images/twitter.png');?>" title="twitter" alt="twitter"/></a>
				<a href="#"><img width="60" height="60" src="<?php echo base_url('assets/bootshop/themes/images/youtube.png');?>" title="youtube" alt="youtube"/></a>
			 </div> 
		 </div>
		<p class="pull-right">&copy; Bootshop</p>
	</div><!-- Container End -->
	</div>
<!-- Placed at the end of the document so the pages load faster ============================================= -->
	<script src="<?php echo base_url('assets/bootshop/themes/js/jquery.js');?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/bootshop/themes/js/bootstrap.min.js');?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/bootshop/themes/js/google-code-prettify/prettify.js');?>"></script>
	
	<script src="<?php echo base_url('assets/bootshop/themes/js/bootshop.js');?>"></script>
    <script src="<?php echo base_url('assets/bootshop/themes/js/jquery.lightbox-0.5.js');?>"></script>
	
	<!-- Themes switcher section ============================================================================================= -->
</body>
</html>