<!DOCTYPE html>
<html lang="en">
  <head>
    <?php $this->load->view('header');?>
  </head>
<body>

  <!-- ***** Preloader Start ***** -->



<!-- Navbar ================================================== -->
<?php $this->load->view('navbar');?>
<!-- Header End====================================================================== -->

	
<!-- Sidebar ================================================== -->
	
<!-- Sidebar end=============================================== -->

  <div class="main-banner" id="top">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="row">
            <div class="col-lg-6 align-self-center">
              <div class="owl-carousel owl-banner">
                <div class="item header-text">
                  <h6>Welcome To Majoo Technology</h6>
                  <h2>Build <em>your </em> website <span>SEO</span>?</h2>
                  <p>Get your best website.</p>
                  <div class="down-buttons">
                    <div class="main-blue-button-hover">
                      <a href="#contact">GET NOW !</a>
                    </div>
                    <div class="call-button">
                      <a href="#"><i class="fa fa-phone"></i> 010-020-0340</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
<div id="about" class="our-services section">
    <div class="services-right-dec">
      <img src="<?php echo base_url('asset/assets/images/services-right-dec.png');?>" alt="">
    </div>
    <div class="container">
      <div class="services-left-dec">
        <img src="<?php echo base_url('asset/assets/images/services-left-dec.png');?>" alt="">
      </div>
      <div class="container">
      <div class="row">
        <div class="col-lg-6 offset-lg-3">
          <div class="section-heading">
            <h2>Select a suitable <em>plan</em> for your next <span>projects</span></h2>
            <span>Our Plans</span>
          </div>
        </div>
      </div>
	  <div class="center">
	  <div class="span10 offset1">
              <ul class="center">
			  <?php foreach($produk as $produk): ?>
				<li class="span3">
				<div class="row">
				<div class="col-lg-6">
				  <div class="item">
				  <p style="min-height:160px;">
					<a  href="<?php echo site_url('welcome/detailproduk/'.$produk->id);?>">
						<?php 
							$product_image = ['src'=>'uploads/' . $produk->gambar,
								'width'=>'160',
								'height'=>'160'];
							echo img($product_image);
						?>
					</a>
					</p>
					<div class="caption">
					  <h5><?php echo $produk->brand;?> <?php echo $produk->model;?></h5>
					 
					 
					  <h4 style="text-align:center"><div class="main-red-button-hover"><a href="<?php echo site_url('welcome/detailproduk/'.$produk->id);?>"> <i class="icon-zoom-in"></div></i></a> <div class="main-blue-button-hover"><a  href="<?php echo site_url('welcome/add_to_cart/'.$produk->id);?>">Add to <i class="icon-shopping-cart"></i></div></a><a href="#">Rp. <?php echo number_format($produk->harga,0,',','.');?></a></h4>
					</div>
				  </div>
				</li>
				<?php endforeach;?>
			  </ul>	
            </div>
			</div>
			</div>
			</div>
			</div>
     
		</div>
   

  </div>
	</div>
</div>
<!-- Footer ================================================================== -->
	<div  id="footerSection">
	<div class="container">
		<div class="row">
			<div class="span3">
				<h5>ACCOUNT</h5>
				<a href="login.html">YOUR ACCOUNT</a>
				<a href="login.html">PERSONAL INFORMATION</a> 
				<a href="login.html">ADDRESSES</a> 
				<a href="login.html">DISCOUNT</a>  
				<a href="login.html">ORDER HISTORY</a>
			 </div>
			<div class="span3">
				<h5>INFORMATION</h5>
				<a href="contact.html">CONTACT</a>  
				<a href="register.html">REGISTRATION</a>  
				<a href="legal_notice.html">LEGAL NOTICE</a>  
				<a href="tac.html">TERMS AND CONDITIONS</a> 
				<a href="faq.html">FAQ</a>
			 </div>
			<div class="span3">
				<h5>OUR OFFERS</h5>
				<a href="#">NEW PRODUCTS</a> 
				<a href="#">TOP SELLERS</a>  
				<a href="special_offer.html">SPECIAL OFFERS</a>  
				<a href="#">MANUFACTURERS</a> 
				<a href="#">SUPPLIERS</a> 
			 </div>
			<div id="socialMedia" class="span3 pull-right">
				<h5>SOCIAL MEDIA </h5>
				<a href="#"><img width="60" height="60" src="<?php echo base_url('assets/bootshop/themes/images/facebook.png');?>" title="facebook" alt="facebook"/></a>
				<a href="#"><img width="60" height="60" src="<?php echo base_url('assets/bootshop/themes/images/twitter.png');?>" title="twitter" alt="twitter"/></a>
				<a href="#"><img width="60" height="60" src="<?php echo base_url('assets/bootshop/themes/images/youtube.png');?>" title="youtube" alt="youtube"/></a>
			 </div> 
		 </div>
		<p class="pull-right">&copy; Bootshop</p>
	</div><!-- Container End -->
	</div>
	

  </div>
<!-- Placed at the end of the document so the pages load faster ============================================= -->
	<script src="<?php echo base_url('assets/bootshop/themes/js/jquery.js');?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/bootshop/themes/js/bootstrap.min.js');?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/bootshop/themes/js/google-code-prettify/prettify.js');?>"></script>
	 
  
	<script src="<?php echo base_url('assets/bootshop/themes/js/bootshop.js');?>"></script>
	
    <script src="<?php echo base_url('assets/bootshop/themes/js/jquery.lightbox-0.5.js');?>"></script>
	  <script src="<?php echo base_url('asset/assets/js/custom.js');?>"></script>
  <script src="<?php echo base_url('asset/assets/js/animation.js');?>"></script>
  <script src="<?php echo base_url('asset/assets/js/imagesloaded.js');?>"></script>
	 <script>
  // Acc
    $(document).on("click", ".naccs .menu div", function() {
      var numberIndex = $(this).index();

      if (!$(this).is("active")) {
          $(".naccs .menu div").removeClass("active");
          $(".naccs ul li").removeClass("active");

          $(this).addClass("active");
          $(".naccs ul").find("li:eq(" + numberIndex + ")").addClass("active");

          var listItemHeight = $(".naccs ul")
            .find("li:eq(" + numberIndex + ")")
            .innerHeight();
          $(".naccs ul").height(listItemHeight + "px");
        }
    });
  </script>
	<!-- Themes switcher section ============================================================================================= -->
</body>
</html>